Home temperature monitoring
===========================

This setup uses the following hardware:
- Raspberry PI 3b+
- DHT11 temperature and humidity sensor

 The following software will be used the accomplish the monitoring.
 - Grafana
 - InfluxDB
 - Python API with flask to receive data from multiple sensors
 - Python script to read out the sensor and send it to the API

Design
------

```
+-----+----+    +---+    +-----+----+
| RPI | DHT+--->+API+<---+ RPI | DHT|
+-----+----+    +-+-+    +-----+----+
                  |
                  |
              +---v----+   +---------+
              |InfluxDB+<--+Grafana  |
              +--------+   +---------+

