import time
import sys
import datetime
from influxdb import InfluxDBClient
from typing import NamedTuple

from flask import (
    Flask,
    request,
    jsonify,
    render_template
)

# Configure InfluxDB connection variables
host = "influxdb" # influx container
port = 8086 # default port
user = "rpi" # the user/password created for the pi, with write access
password = "rpi" 
dbname = "sensor_data" # the database 
interval = 60 # Sample period in seconds

# Create the InfluxDB client object
client = InfluxDBClient(host, port, user, password, dbname)

# Create the application instance
app = Flask(__name__, template_folder="templates")
app.config["DEBUG"] = True

# Create a URL route in our application for "/"
@app.route('/', methods=['GET'])
def home():
    """
    This function just responds to the browser URL
    localhost:5000/

    :return:        the rendered template 'home.html'
    """
    return render_template('home.html')


@app.route('/api/v1/resources/sensors/all', methods=['GET'])
def api_all():
    databases = client.query('show measurements')

    return jsonify(databases.raw)


@app.route('/api/v1/sensor', methods=['POST'])
def _send_sensor_data_to_influxdb():

    content = request.json
        
    # Read the posted data 
    measurement = content.get('sensor_id')
    location = content.get('location')
    humidity = content.get('humidity')
    temperature = content.get('temperature')
    iso = time.ctime()
    
    # Create the JSON data structure
    json_body = [
        {
          "measurement": measurement,
              "tags": {
                  "location": location,
              },
              "time": iso,
              "fields": {
                  "temperature" : temperature,
                  "humidity": humidity
              }
          }
        ]
    client.write_points(json_body)

    return jsonify(json_body)


@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0')

