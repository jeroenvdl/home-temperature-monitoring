!/bin/bash

SENSOR_ID=rpi-dht11
TEMPERATURE=$(shuf -i 20-25 -n 1).$(shuf -i 20-25 -n 1)
HUMIDITY=$(shuf -i 35-40 -n 1).$(shuf -i 35-40 -n 1)
LOCATION=home
URL=http://localhost/api/v1/sensor

# echo $SENSOR_ID
# echo $TEMPERATURE
# echo $HUMIDITY
# echo $LOCATION
# echo $URL


JSON_STRING=$( jq -n \
                 --arg sid "$SENSOR_ID" \
                 --argjson tem $TEMPERATURE \
                 --argjson hum $HUMIDITY \
                 --arg loc "$LOCATION" \
                 '{"sensor_id":$sid, "temperature":$tem, "humidity":$hum, "location":$loc}')

# echo $JSON_STRING
# curl -X POST -d "$JSON_STRING" -H "Content-Type: application/json" $URL

#while true; do curl -X POST -d '{"sensor_id":"rpi-dht11", "temperature":20.2, "humidity":38.5, "location":"home"}' -H "Content-Type: application/json" http://localhost/api/v1/sensor; sleep 10; done
while true; do 
  TEMPERATURE=$(shuf -i 20-35 -n 1).$(shuf -i 0-90 -n 1)
  HUMIDITY=$(shuf -i 35-50 -n 1).$(shuf -i 0-90 -n 1)
  JSON_STRING=$( jq -n \
                    --arg sid "$SENSOR_ID" \
                    --argjson tem $TEMPERATURE \
                    --argjson hum $HUMIDITY \
                    --arg loc "$LOCATION" \
                    '{"sensor_id":$sid, "temperature":$tem, "humidity":$hum, "location":$loc}')

  curl -X POST -d "$JSON_STRING" -H "Content-Type: application/json" $URL
  sleep 10
done

