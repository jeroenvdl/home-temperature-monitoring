#!/usr/bin/python

import sys
import time
import json
import requests
import Adafruit_DHT

url = "http://192.168.1.33/api/v1/sensor"

humidity, temperature = Adafruit_DHT.read_retry(11, 4)

humidity = round(humidity, 2)
temperature = round(temperature, 2)

# give the sensor a name
sensor_id = "rpi-dht11"
# think of measurement as a SQL table, it's not...but...
measurement = "rpi-dht11"
# location will be used as a grouping tag later
location = "meterkast"
# interval between measurements
interval = 10

# Run until you get a ctrl^c
try:
    while True:
        # Read the sensor using the configured driver and gpio
        humidity, temperature = Adafruit_DHT.read_retry(11, 4)

        # Send the JSON data to the api
        payload = { "sensor_id": measurement, "location": location, "humidity": humidity, "temperature": temperature }
        headers = { "Content-Type": "application/json" }
        r = requests.post(url, headers=headers, data=json.dumps(payload))
        print(r.text)
        
        # Wait until it's time to query again...
        time.sleep(interval)
 
except KeyboardInterrupt:
    pass
